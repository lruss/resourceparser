package com.alamon.resource_reader.json;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JsonFileContents {
	private String fileName;
	private List<Map<String, String>> fileContents;

	public JsonFileContents(String fileName) throws Exception {
		this.fileName = fileName;
		try {
			ParseJsonFile fileParser = new ParseJsonFile(fileName);
			fileContents = fileParser.getFileContents();
		} catch (Exception e) {

		}
	}
	
	/**
	 * Convert a collection of Maps representing json file elements 
	 * into a List of String values of a fieldName parameter.
	 * @param fieldName
	 * @return
	 */
	public List<String> convertFileContentsToListOfFiledValues(String fieldName)	{
		List<String> fieldValues = new ArrayList<>();
		for(Map<String, String> element : fileContents)	{
			fieldValues.add(element.get(fieldName));
		}
		return fieldValues;
	}

	public String getFileName() {
		return fileName;
	}

	public List<Map<String, String>> getFileContents() {
		return fileContents;
	}

	/**
	 * This method returns ALL elements that match at of the field/value pairs passed as a map parameter.
	 * NOTES: 
	 * 	- Do NOT include multiple values for the same field in the map - it will return results for the last entry only.
	 *  - Will not work for empty strings.
	 *  - For more complex queries, return all elements and implement appropriate functionality for processing. Sorry.
	 * @param criteria
	 * @return
	 */
	public List<Map<String, String>> getElementsByFieldNameAndValue(Map<String, String> criteria) {
		List<Map<String, String>> matchedElements = new ArrayList<>();
		if (fileContents != null) {
			for (Map<String, String> jsonElement : fileContents) {
				boolean doesMatch = true;
				for (Map.Entry<String, String> criteriaPair : criteria.entrySet()) {
					if (!jsonElement.containsKey(criteriaPair.getKey())
							|| !jsonElement.get(criteriaPair.getKey()).equalsIgnoreCase(criteriaPair.getValue())) {
						// System.out.println("LR TEST: jsonElement = " +
						// jsonElement.get(criteriaPair.getKey()) + ", criteria
						// key
						// = " + criteriaPair.getKey() + ", criteriaValue = " +
						// criteriaPair.getValue());
						doesMatch = false;
					}

				}
				if (doesMatch) {
					matchedElements.add(jsonElement);
				}
			}
		}
		return matchedElements;
	}

	public List<String> getFieldValuesByReferenceFieldNameAndValue(Map<String, String> criteria,
			String fieldToFetchValues) {
		List<Map<String, String>> elements = getElementsByFieldNameAndValue(criteria);
		List<String> values = new ArrayList<>();
		for (Map<String, String> element : elements) {
			if (element != null && element.containsKey(fieldToFetchValues)) {
				if (!element.get(fieldToFetchValues).isEmpty()) {
					values.add(element.get(fieldToFetchValues));
				}
			}
		}
		return values;
	}

	public List<String> getAllValuesByFieldName(String fieldName) {
		List<String> values = new ArrayList<>();
		for (Map<String, String> element : fileContents) {
			values.add(element.get(fieldName));
		}

		return values;
	}

	public List<String> getUniqueValuesByFieldName(String fieldName) {
		Set<String> uniqueValues = new HashSet<>();
		for (String value : getAllValuesByFieldName(fieldName)) {
			uniqueValues.add(value);
		}
		return new ArrayList<String>(uniqueValues);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("\n============ File: " + fileName + " =============\n");
		for (Map<String, String> element : fileContents) {
			// sb.append(element.toString() + "\n");
			for (Map.Entry<String, String> pair : element.entrySet()) {
				sb.append("field: " + pair.getKey() + ", value: " + pair.getValue() + "\n");
			}
		}
		return sb.toString();
	}
}
