package com.alamon.resource_reader.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * This class loads data from all json files listed in the
 * staticResourcesToLoad.json file located in the
 * [PROJECT_ROOT]/static_resources directory (root of jar file) Created by lruss
 * on 6/30/2017.
 */
public class LoadStaticResources {
	private static LoadStaticResources ourInstance = new LoadStaticResources();

	public static LoadStaticResources getInstance() {
		return ourInstance;
	}

	private static Map<String, JsonFileContents> resources = new HashMap<>();

	private LoadStaticResources() {
	}

	/**
	 * Do actual reading/loading of all json resources. This method is not
	 * called explicitly since this is a singleton class and it is run on start
	 * up only when global resources object is populated for future use.
	 */
	private static void doLoad() throws Exception {
		JsonFileContents masterLoader = new JsonFileContents("staticResourcesToLoad.json");
		for (String fileName : masterLoader.getAllValuesByFieldName("name")) {
			JsonFileContents fileContents = new JsonFileContents(fileName);
			resources.put(fileContents.getFileName(), fileContents);
		}
	}

	/**
	 * If resources object is not populated (empty), calls doLoad() method to
	 * populate it and then returns the map element with the fileName passed as
	 * a key
	 * 
	 * @param fileName
	 * @return
	 */
	public static JsonFileContents getFileContentsByFileName(String fileName) throws Exception {
		try {
			if (resources.isEmpty()) {
				doLoad();
			}
		} catch (Exception e) {
			System.out.println("Unable to load file [" + fileName + "], " + e.getMessage());
		}
		if (resources.get(fileName) != null) {
			return resources.get(fileName);
		} else {
			System.out.println("Unable to load file [" + fileName + "]");
			return new JsonFileContents(fileName);
		}
	}

	/**
	 * Convenience method to fetch an element field value from a json file by known field/value pair.<br>
	 * Example:<br>
	 * json file to search in: varConstantsArr.json<br>
	 * {<br>
		"name": "precision",<br>
		"value": "2",<br>
		"description": ""<br>
	}<br>
	Params: <br>
			fileName 			- "varConstantsArr.json"<br>
			referencefieldName 	- "name"<br>
			referenceFieldValue - "precision"<br>
			fieldNameToQuery 	- "value"<br>
	Returns: 2<br>
	 * @param fileName<br>
	 * @param referenceFieldName<br>
	 * @param referenceFieldValue<br>
	 * @param fieldNameToQuery<br>
	 * @return value of the field to query or empty string (not null) if the file or field is not found
	 */
	public static String getFieldValue(String fileName, String referencefieldName, String referenceFieldValue,
			String fieldNameToQuery) {
		Map<String, String> criteria = new HashMap<>();
		criteria.put(referencefieldName, referenceFieldValue);
		try {
			return getFileContentsByFileName(fileName).getElementsByFieldNameAndValue(criteria).get(0)
					.get(fieldNameToQuery);
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * This method is similar to {@link #getFieldValue()}, with the following differences:
	 * It returns values of ALL queried fields 
	 * @param fileName
	 * @param referencefieldName
	 * @param referenceFieldValue
	 * @param fieldNameToQuery
	 * @return
	 */
	public static List<String> getAllFieldValues(String fileName, String referencefieldName, String referenceFieldValue,
			String fieldNameToQuery) {
		Map<String, String> criteria = new HashMap<>();
		criteria.put(referencefieldName, referenceFieldValue);
		List<String> values = new ArrayList<>();
		try {
			for (Map<String, String> element : getFileContentsByFileName(fileName)
					.getElementsByFieldNameAndValue(criteria)) {
				values.add(element.get(fieldNameToQuery));
			}

		} catch (Exception e) {
		}
		return values;
	}

	public static List<String> getUniqueValuesByFieldName(String fileName, String referencefieldName,
			String referenceFieldValue, String fieldNameToQuery) {
		return new ArrayList<String>( new HashSet<String>(
				getAllFieldValues(fileName, referencefieldName, referenceFieldValue, fieldNameToQuery)));
	}

	public static String print() {
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String, JsonFileContents> pair : resources.entrySet()) {
			sb.append(pair.getValue().toString());
		}
		return sb.toString();
	}

}