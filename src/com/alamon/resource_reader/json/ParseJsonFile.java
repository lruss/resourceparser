package com.alamon.resource_reader.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;

public class ParseJsonFile {
	private String fileName;

	private List<Map<String, String>> fileContents = new ArrayList<>();
	private List<List<Map<String, String>>> fileObjects = new ArrayList<>();

	public ParseJsonFile(String fileName) {
		this.fileName = fileName;
		parseJsonFile();
	}

	private void parseJsonFile() {
		GetResourceFile getResourceFile = new GetResourceFile();
		JsonNode jsonNode = getResourceFile.getJsonNode(fileName);		
		// Json data is array
		if (!jsonNode.getFields().hasNext()) {
			processJsonArray(jsonNode.toString());
		}
		// Json data is an object with multiple nodes
		else {
			// System.out.println("LR TEST JSON NODE OBJ = " +
			// jsonNode.toString());
			readJsonObjData(jsonNode);
		}
	}

	private void readJsonObjData(JsonNode jsonNode) {
		// System.out.println("LR TEST: new elements Map");
		Map<String, String> fields = new HashMap<>();

		Iterator<Map.Entry<String, JsonNode>> ite = jsonNode.getFields();
		while (ite.hasNext()) {
			Map.Entry<String, JsonNode> entry = ite.next();

			if (entry.getValue().isObject()) {
				// System.out.println("LR TEST node obj map: " +
				// entry.toString());
				readJsonObjData(entry.getValue());
			} else {
				// System.out.println("LR TEST node obj1 map: " +
				// entry.toString());
				fields.put(entry.getKey(), entry.getValue().toString());
				// System.out.println("key:" + entry.getKey() + ", value:" +
				// entry.getValue());

			}
		}

		// resources.add(fields);
	}

	public Map<String, String> processElement(String element) {
		Map<String, String> pairsMap = new HashMap<>();
		parseJsonElements(element);
		for (String pair : element.split(",")) {
			String[] keyValue = pair.split(":");
//			System.out.println("LR TEST key = " + keyValue[0].replaceAll("\"", "") + ", value = "
//					+ keyValue[1].replaceAll("\"", ""));
			pairsMap.put(keyValue[0].replaceAll("\"", ""), keyValue[1].replaceAll("\"", ""));
		}
		return pairsMap;
	}

	public void parseJsonElements(String elementStr) {
		List<Map<String, String>> jsonElement = new ArrayList<>();
		Map<String, String> elementMap = new HashMap<>();
		for (String pair : elementStr.split(",")) {
			//System.out.println("LR TEST element = " + pair);
			String[] keyValue = pair.split(":");			
			elementMap.put(keyValue[0].replaceAll("\"", ""), keyValue[1].replaceAll("\"", ""));			
			jsonElement.add(elementMap);
		}
		fileObjects.add(jsonElement);
	}

	private void processJsonArray(String jsonNodeStr) {
		if (jsonNodeStr.indexOf("}") == -1) {
			return;
		}
		String element = jsonNodeStr.substring(jsonNodeStr.indexOf("{") + 1, jsonNodeStr.indexOf("}"));
		//System.out.println("element = " + element);
		fileContents.add(processElement(element));
		processJsonArray(jsonNodeStr.substring(element.length() + 3));
	}

	public List<Map<String, String>> getFileContents() {
		return fileContents;
	}

	public List<List<Map<String, String>>> getFileObjects() {
		return fileObjects;
	}
	
	

}
