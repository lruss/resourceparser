package com.alamon.resource_reader.json;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

public class GetResourceFile {
	private static final String tempFilePrefix = "i-calc_";
	public JsonNode getJsonNode(String fileName) {
		File tempFile = null;
		JsonNode jsonNode = null;
		FileOutputStream out = null;
		try (InputStream is = getClass().getClassLoader().getResourceAsStream(fileName);) {
			tempFile = File.createTempFile(tempFilePrefix, "");			
			out = new FileOutputStream(tempFile);
			tempFile.deleteOnExit();
			IOUtils.copy(is, out);

			JsonParser jp = null;
			JsonFactory jsonFactory = new JsonFactory();
			jp = jsonFactory.createJsonParser(tempFile);
			ObjectMapper objectMapper = new ObjectMapper();
			jp.setCodec(objectMapper);
			jsonNode = jp.readValueAsTree();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				out.close();
				tempFile.delete();			
				deleteTempFiles(tempFile)	;
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}

		return jsonNode;
	}
	
	/**
	 * Delete all files with the "i-calc_" prefix in the temp directory.
	 * Windows is inconsistent in this regard.
	 * It should not be necessary since every file is deleted once it's read into memory but just in case.
	 * @param tempFile
	 */
	private void deleteTempFiles(File tempFile)		{
		final File dir = new File(tempFile.getAbsoluteFile().getParent());
	    final String[] allFiles = dir.list();
	    for (final String file : allFiles) {
	        if (file.startsWith(tempFilePrefix)) {
	            new File(tempFile.getAbsoluteFile().getParent() + File.separatorChar + file).delete();	            
	        }
	    }
	}

}
