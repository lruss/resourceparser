package com.alamon.resource_reader.tests;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alamon.resource_reader.json.JsonFileContents;
import com.alamon.resource_reader.json.LoadStaticResources;

import jdk.nashorn.internal.ir.annotations.Ignore;

/**
 * Created by lruss on 6/27/2017.
 */
@SuppressWarnings("unused")
public class ResourseParserTest {
	// @Test
	@Ignore
	public void testFileLoader() throws Exception {
		LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json");
		// System.out.println(LoadStaticResources.print());

	}

	// @Test
	@Ignore
	public void testGetFileContentsByFileName() throws Exception {
		System.out.println("\n================= testGetFileContentsByFileName() ======================\n");
		JsonFileContents fileContents = LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json");
		System.out.println(fileContents.getFileContents());
	}

	// @Test
	@Ignore
	public void testMatchedElemetsAND() throws Exception {
		System.out.println("\n================= testMatchedElemets() ======================\n");
		Map<String, String> criteria = new HashMap<>();
		// criteria.put("TAPER mm-m", "24");
		// criteria.put("TREATMENT CODE", "D");
		criteria.put("SPECIES CLASS", "PINE");
		// criteria.put("SPECIES SUBCLASS", "LONGLEAF");
		// criteria.put("TREATMENT CODE", "D");
		// criteria.put("SPECIES CLASS", "LARCH");
		// criteria.put("SPECIES SUBCLASS", "WESTERN");
		JsonFileContents fileContents = LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json");
		for (Map<String, String> element : fileContents.getElementsByFieldNameAndValue(criteria)) {
			System.out.println(element);
		}
	}

	// @Test
	@Ignore
	public void getFieldValuesByReferenceFieldNameAndValue() throws Exception {
		System.out.println(
				"\n================= \ngetFieldValuesByReferenceFieldNameAndValue() from woodSpeciesArr.json:  \n"
						+ "Field SPECIES CLASS, value: PINE, values from field SPECIES SUBCLASS\n======================\n");
		JsonFileContents fileContents = LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json");
		Map<String, String> criteria = new HashMap<>();
		criteria.put("SPECIES CLASS", "PINE");
		for (String value : fileContents.getFieldValuesByReferenceFieldNameAndValue(criteria, "SPECIES SUBCLASS")) {
			System.out.println("value: " + value);
		}
	}

	// @Test
	@Ignore
	public void getAllValuesByFieldName() throws Exception {
		JsonFileContents fileContents = LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json");
		for (String value : fileContents.getAllValuesByFieldName("SPECIES CLASS")) {
			System.out.println("value: " + value);

		}
	}

	// @Test
	// @Ignore
	public void getAllTaperValues() throws Exception {
		JsonFileContents fileContents = LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json");
		for (String value : fileContents.getAllValuesByFieldName("TAPER in-ft")) {
			System.out.println("value: " + value);

		}
	}

	// @Test
	@Ignore
	public void getUniqueValuesByFieldName() throws Exception {
		JsonFileContents fileContents = LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json");
		for (String value : fileContents.getUniqueValuesByFieldName("SPECIES CLASS")) {
			System.out.println("value: " + value);

		}
	}

	// @Test
	// @Ignore
	public void getNamesFromVarContantsArrJson() throws Exception {
		if (LoadStaticResources.getFileContentsByFileName("varConstantsArr.json")
				.getAllValuesByFieldName("name") == null) {
			System.out.println("Unable to load varConstantsArr.json");
		}
		for (String value : LoadStaticResources.getFileContentsByFileName("varConstantsArr.json")
				.getAllValuesByFieldName("name")) {
			System.out.println(value);
		}
	}

	// @Test
	// @Ignore
	public void getTapersFromWoodSpeciesArrJson() throws Exception {
		if (LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json")
				.getAllValuesByFieldName("TAPER in-ft") == null) {
			System.out.println("Unable to load varConstantsArr.json");
		}
		for (String value : LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json")
				.getAllValuesByFieldName("TAPER in-ft")) {
			System.out.println(value);
		}
	}

	// @Test
	@Ignore
	public void getPrecisionFromVarContantsArrJson() throws Exception {
		Map<String, String> criteria = new HashMap<>();
		criteria.put("name", "precision");
		System.out.println(LoadStaticResources.getFileContentsByFileName("varConstantsArr.json")
				.getElementsByFieldNameAndValue(criteria).get(0).get("value"));
	}

	// @Test
	@Ignore
	public void getPrecisionFromVarContantsArrJsonShort() {
		System.out.println(LoadStaticResources.getFieldValue("varConstantsArr.json", "name", "precision", "value"));
	}

	@Test
	// @Ignore
	public void getNoOfCordsFromVarContantsArrJsonShort() {
		System.out.println("NoOfCords = " + LoadStaticResources.getFieldValue("varConstantsArr.json", "name", "noOfChords", "value"));
	}

	// @Test
	// @Ignore
	public void getAllFieldValues() {
		System.out.println(LoadStaticResources.getAllFieldValues("woodSpeciesArr.json", "SPECIES CLASS", "PINE",
				"SPECIES SUBCLASS"));
	}

	// @Test
	public void getTaperBySpecies() throws Exception {
		Map<String, String> criteria = new HashMap<>();
		criteria.put("SPECIES CLASS", "PINE");
		criteria.put("SPECIES SUBCLASS", "LODGEPOLE");
		System.out.println(LoadStaticResources.getFileContentsByFileName("woodSpeciesArr.json")
				.getElementsByFieldNameAndValue(criteria).get(0).get("TAPER in-ft"));
	}

	@Test
	public void getUniqueValuesByFieldNameAndValue() {
		for (String value : LoadStaticResources.getUniqueValuesByFieldName("woodSpeciesArr.json", "SPECIES CLASS",
				"FIR", "SPECIES SUBCLASS")) {
			System.out.println("value: " + value);
		}
	}
}
